# Sandbox

## Frontend

Frontend sandbox runs in the browser window using `<iframe>` tag, with all 
JS scripts, CSS files and main.wasm loaded as blobs. 

During loading process, Play Seed would process links to JS files 
from `.play-seed/main.html` and substitutes their `src` to blob url. 
Then JS files would be processed recursively, so if one file uses `import`
to connect another file, it would also be loaded.

CSS loading is currently simpler and in case if CSS file links 
another file, it would be ignored. CSS files are loaded by `href` attribute 
in `<link>` of `.play-seed/main.html`.

## Backend

Backend build sandbox using `docker` to compile sources you've entered under `src` folder.
Only this folder would be mounted to docker container.
Everything outside it, would be ignored during build. This also includes, for example `Cargo.toml`. 
Backend would use it's own `Cargo.toml` and does not allow currently to change it. 
Same goes for `build.rs`.

Build would call `wasm-pack` command with debug mode enabled (because it is the fastest currently). 
The whole compilation would timeout of after about 20 seconds. If timeouted, you might see some unclear
error output. In addition to timeout, backend applies rate limiting, when no more than 3 in-flight requests
are allowed in general. 

### Dependencies

Currently `Cargo.toml` has a number of precompiled dependencies, which could be used in the code. You can see 
current version in the sources [here](https://gitlab.com/strwrite/seed-playground/-/blob/master/compiler/seed/Cargo.toml).
As of now there's no way to add anything to these dependencies except make merge request to that file. 
Before doing that, you might want to communicate with me (see [Contacts](./contacts.md)).
