# About [Play Seed](https://ide.play-seed.dev)

[Seed](https://seed-rs.org) is a frontend Rust framework for creating fast and reliable web apps with an elm-like architecture.

[Play Seed](https://ide.play-seed.dev) is a playground, where you can write simple Seed based application in the browser, build it and run.

## Sources

You can find sources in the Gitlab: [frontend](https://gitlab.com/strwrite/seed-playground-ide) and [backend](https://gitlab.com/strwrite/seed-playground).

## Acknowledgements

Play Seed has been forked from two base sources:

- [WebAssemblyStudio](https://github.com/wasdk/WebAssemblyStudio) used for a frontend
- [Rust Playground](https://github.com/integer32llc/rust-playground) used for a backend

Without those projects Play Seed would've never be created.