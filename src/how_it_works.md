# How it works

In the [playground](https://ide.play-seed.dev) you will see three buttons: 'Run', 'Build' and 'Build & Run'.

## Build 
'Build' would cause `.play-seed/build.ts` script execution, which by default would grab `src` folder and send it to backend for compilation. There is predefined `Cargo.toml` file, which backend sandbox would use to compile received sources with standard 'wasm-pack' call, the results of which (wasm and js files) would be returned back to the frontend app and put in the `out` folder in the playground. Also, once build is finished you will see stdout and stderr results in the Output panel. 
If you want to know more about backend sandbox, check `Backend` in [Sandbox](./sandbox.md/).

## Run

'Run' would grab `.play-seed/main.html` and use it as an entrypoint, populating iframe with it's content and additionally loading any JS or CSS files linked from it into same iframe. This loading process, however, is currently imperfect (see [Sandbox](./sandbox.md)). By default `main.html` would link `main.js`, which in its turn would load wasm-bindgen built js from 'out/index.js' and pass loaded 'out/main.wasm' to it.

'Run' is blocked until build happens at least once.

# Build and Run

As you have probably guessed, 'Build & Run' would simply run both 'Build' and 'Run' sequentially. As build process might be somewhat long, if you don't change Rust code, you could use only 'Run' to see new result.

# Limits

Additionally you might need to know, that currently backend allows only three simultaneous builds running, as they are somewhat heavy and compute capabilities are quite limited. In case if limit is exceeded you will see message 'max connections reached' in the Output.
