# Github Integration

## Open from repository

GitHub repositories could be specified in the `github` parameter of URL to playground like this:
[https://ide.play-seed.dev/?github=MartinKavik/seed-app-counter](https://ide.play-seed.dev/?github=MartinKavik/seed-app-counter).

When playground is opened from such URL, it would try to load sources from 
that repository. Currently by default only `src` folder would be loaded. 
However, that could be configured, see [Configuration](#configuration) below 
for details.

To integrate any repository with Play Seed, during loading process three standard files 
would be also added into playground under `.play-seed` directory - `main.html`, `main.js` 
and `build.ts`. Those files would be added only if they were not present in the repository.
Counter example linked above is one of those simple apps, which are compatible with standard 
Play Seed entrypoint. However, other repositories might need to override stadart project
bootstrap by creating those files in the repository. You might also want to only override some of 
those files, then playground would fill the rest. 

## Configuration

Before loading the sources, Play Seed would try to see if repository contains
configurations in `.play-seed/config.yml` file. If the file is present, it would
be recognized and used during sources loading. 

Currently only one configuration option is supported there - `directories` array
allows you to choose which directories to load into playground. 

Here's an example how to add `css` folder in addition to default `src`: 

``` yaml
directories: 
  - src # default 
  - css # additional
```

## Rate limit

GitHub limits the number of requests made anonymously, so if first request failed with message
about rate limit, playground would request you to authorize it to make requests
on your behalf, which grants much more loose restriction.

Additionally you will be asked authorization to create gists.

## Gists

You can press right button in the file tree and select to create a Gist from files currently
saved in playground or use button "Create Gist" in the toolbar.
That would require authorization to GitHub. 

If you place id of your Gist in the url, for example like this: 
```
https://ide.play-seed.dev/?g=5d53999f5d75f9b5526340ae4b99cf38
```
, Play Seed would try to load sources from Gist. 

It does not support loading truncated Gists yet.

Url of your currently opened sources would also have `g` after you create Gist. 
To share created Gist with others, you can just copy url from browser 
and send it to someone to discuss.
