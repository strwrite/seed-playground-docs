# Roadmap

## More dependencies

Initial backend from Rust Playground supported building compiler 
image with 100 most popular crates. Need to support that too 
in addition to all crates required for Seed and examples. 

## Integrate with router

Right now router from Seed would not work in playground, because
it tries to access url, but playground sandbox runs in frame, 
which does not have access to url and even if it had, it is 
not correct to change playground url to keep state of app in
iframe. History API should be either stubbed and mapped to different
state or at least mocked to prevent runtime errors. 

## Send compile errors to Problems

When compiler failed, need to highlight relevant code and display
errors in Problems tab. UI seems to have that functionality 
already, but it was not integrated with backend yet.

## Embedding

There should be a possibility to embed playground into any website
in order to support two functionalities: "Open in IDE" and "Run and Render".
Iframe which produced should support options to display either only one of 
buttons and/or code.

### Open in IDE

Should simply open code from embedded iframe in the IDE.
It's an open question how to send data from website to frontend.
Might need to temporary save it via backend.
Alternatively could just use Gist for storage.

### Run and Render

Should communicate with Playground API to build and then 
run in iframe right on the embedding website.

## Complete CI/CD

Currently when backend change is done, some manual actions over SSH
are required, such as download image (if compiler was changed) 
or download binary and restart service. Those are already scripted,
but not fully automated. They should be run from Gitlab CI as well
as builds.

## Mobile layout

Even though monaco-editor used in UI does not support mobile, we 
can still adjust layout to make it at least somehow adaptive.



